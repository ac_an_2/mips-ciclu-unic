----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/01/2021 11:09:12 AM
-- Design Name: 
-- Module Name: Instruction Decode - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity InstructionDecode is
    Port ( Instr : in STD_LOGIC_VECTOR (15 downto 0);
           RegWrite : in STD_LOGIC;
           RegDst : in STD_LOGIC;
           ExtOp : in STD_LOGIC;
           clk : in STD_LOGIC;
           WD : in STD_LOGIC_VECTOR (15 downto 0);
           RD1 : out STD_LOGIC_VECTOR (15 downto 0);
           RD2 : out STD_LOGIC_VECTOR (15 downto 0);
           Ext_Imm : out STD_LOGIC_VECTOR (15 downto 0);
           funct : out STD_LOGIC_VECTOR (2 downto 0);
           sa : out STD_LOGIC);
end InstructionDecode;

architecture Behavioral of InstructionDecode is
type stocareOperanzi is array(0 to 7) of std_logic_vector(15 downto 0);
signal readAdress1 :std_logic_vector(2 downto 0);
signal readAdress2 :std_logic_vector(2 downto 0);
signal WriteAdress :std_logic_vector(2 downto 0);
signal extend : std_logic_vector(8 downto 0);
signal extendAux: std_logic_vector(8 downto 0);
signal RegFile :stocareOperanzi:=(
x"0000",
x"0007",
x"0006",
x"0005",
x"0004",
x"0003",
x"0002",
x"0001",
others=>x"0000"
);
begin
readAdress1<=Instr(12 downto 10);
readAdress2<=Instr(9 downto 7);
WriteAdress<=Instr(6 downto 4) when RegDst='1' else Instr(9 downto 7);
extendAux<="111111111" when Instr(6)='1' else "000000000";
extend<="000000000" when ExtOp='0' else extendAux;
Ext_Imm(15 downto 7)<=extend;
Ext_Imm(6 downto 0)<=Instr(6 downto 0);
funct<=Instr(2 downto 0);
sa<=Instr(3);
process(readAdress1,readAdress2)
begin
RD1<=RegFile(conv_integer(readAdress1));
RD2<=RegFile(conv_integer(readAdress2));
end process;

process(clk)
begin
if clk='1' and clk'event then
if RegWrite='1' then
RegFile(conv_integer(WriteAdress))<=WD;
end if;
end if;
end process;
end Behavioral;
