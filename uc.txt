----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/01/2021 11:55:22 AM
-- Design Name: 
-- Module Name: UC - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UC is
    Port ( Instr : in STD_LOGIC_VECTOR (2 downto 0);
           RegDst : out STD_LOGIC;
           ExtOp : out STD_LOGIC;
           ALUSrc : out STD_LOGIC;
           Branch : out STD_LOGIC;
           Jump : out STD_LOGIC;
           MemWrite : out STD_LOGIC;
           MemtoReg : out STD_LOGIC;
           RegWrite : out STD_LOGIC;
           ALUOp : out  STD_LOGIC_VECTOR(2 downto 0)
    
    );
end UC;

architecture Behavioral of UC is

begin
process(Instr)
begin
RegDst<='0';
ExtOp<='0';
ALUSrc<='0';
Branch<='0';
Jump<='0';
MemWrite<='0';
MemtoReg<='0';
RegWrite<='0';
ALUOp<="000";

 case Instr is
 when "000" => ALUOp<="000"; RegDst<='1'; RegWrite<='1';
 when "001" => ALUOp<="001"; RegWrite<='1'; ALUSrc<='1'; ExtOp<='1';  
 when "010" => ALUOp<="010"; RegWrite<='1'; ALUSrc<='1'; ExtOp<='1'; MemtoReg<= '1';
 when "011" => ALUOp<="011"; ALUSrc<='1'; ExtOp<='1'; MemWrite<='1';
 when "100" => ALUOp<="100"; ExtOp<='1'; Branch<='1';
 when "101" => ALUOp<="101"; RegWrite<='1'; ALUSrc<='1'; ExtOp<='1';
 when "110" => ALUOp<="110"; RegWrite<='1'; ALUSrc<='1'; ExtOp<='1';
 when "111" => ALUOp<="111"; Jump<='1';
 when others => null;
end case; 
end process;

end Behavioral;
